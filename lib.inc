section .text
 
     ; --- CALLEE-SAVED REGISTERS --- 
    ; Любая функция является callee-saved (т.к. функции пишутся для того, чтоб их вызывать). 
    ; Это значит, что в начале любой ф. мы должны 'push'ить callee-saved регистры (только те, кот. используются в 
    ; нашей конкретной программе, но всего их 7: rbx, rbp, rsp, r12-r15), а в конце - 'pop'ить их. (или гарантровать их
    ; их правильное значение а выходе другим способом, напр. rsp в print_uint)
    ; --- CALLER-SAVED REGISTERS --- 
    ; If our function invokes commands 'call' or 'syscall', these registers should be
    ; saved before invoking a function and restored after. One does not have to save and restore them if their value 
    ; will not be of importance after.
    ; --- ALL OTHER REGISTERS ---
    ; All other registers are caller-saved. 

; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60; exit system call в rax, код возврата уже в rdi
    syscall



; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax; rax будет содержать длину строки. Сначала нужно его обнулить
    .loop:
        cmp byte [rdi + rax], 0; проверяет, является ли текущий символ нуль-терминатором
        je .end
        inc rax
        jmp .loop
    .end:
    ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax; длина строки rax -> rdx (равняется кол-ву байт для вывода)
    mov rax, 1; write system call -> rax
    mov rsi, rdi; адрес первого байта последовательности, кот. нужно вывести -> rsi
    mov rdi, 1; stdout (1) -> rdi
    syscall
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA



; Принимает код символа и выводит его в stdout
print_char:
    mov rdx, 1; rdx готов! (хотим вывести только 1 байт)
    mov rax, 1; rax готов! (write system call в rax) 
    dec rsp
    mov rsi, rsp ; rsi готов!
    mov rcx, rdi; входное значение rdi -> rcx
    mov byte [rsi], cl; младшие 8 бит rcx -> на вершину стека
    mov rdi, 1; rdi готов! (stdout (1) в rdi)
    syscall
    inc rsp
    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
%define MAX_symbols_in_format10 24; взяли с запасом (для беззнакового 8-байтового числа их 19, для знакового - 20)
print_uint:
    mov rax, rdi; входное число в rdi -> rax
    mov rdi, rsp; в rdi - указатель на нижнюю ячейку выходного массива символов
    sub rsp, MAX_symbols_in_format10 

    mov rcx, 10; 10 с.с.
    mov rdx, 0

    ;--- блок формирования выходного массива символов ---
    dec rdi;!!!  
    mov byte [rdi], 0; символ конца строки (нуль-терминатор) -> нижнюю ячейку выходного массива символов

    .loop:
        div rcx; unsigned divide RDX:RAX by r/m64, with result stored in RAX := Quotient(частное), RDX := Remainder(остаток)

        add rdx, '0'; числовой остаток превращается в соответствующий ASCII-код
        dec rdi
        mov byte [rdi], dl
        
        cmp rax, 0
        jz .end
        cmp rax, 10
        js .last_symbol
        mov rdx, 0; т.к. unsigned divide RDX:RAX by r/m64 (RDX:RAX - 16-байтное значение)
        jmp .loop

    .last_symbol:
    add rax, '0' 
    dec rdi
    mov byte [rdi], al
    ; --- конец блока ---

    .end:
    call print_string
    add rsp, MAX_symbols_in_format10
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0; проверяем знак пришедшего к нам знакового 8-байтового числа
    js .negative
    call print_uint
    jmp .end

    .negative:
    push rdi
    mov rdi, '-'; код символа минус
    call print_char; вывод символа минус
    pop rdi

    neg rdi
    call print_uint

    .end:
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rcx, 0
    .loop:
        mov r9b, byte [rdi + rcx]; r9b - младший байт регистра r9
        cmp r9b, byte [rsi + rcx]

        jnz .not_equal
        cmp byte [rdi + rcx], 0; проверяет, был ли сравнимаемый символ нуль-терминатором
        jz .equal; если это был нуль-терминатор, то останавливаем сравнение символов
        inc rcx
        jmp .loop

    .not_equal:
    mov rax, 0
    jmp .end

    .equal:
    mov rax, 1

    .end:
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char: 
    mov rax, 0; rax готов! (read system call)
    mov rdi, 0; rdi готов! (0 - stdin)
    dec rsp
    mov rsi, rsp; rsi готов! (address of the first byte, the received bytes will be placed there)
    mov rdx, 1; rdx готов! (хотим прочитать только 1 байт)
    syscall

    cmp rax, 0; проверка на конец потока (при вызове read syscall регистр rax содержит 'number of bytes successfully read')          
    jz .end

    mov al, byte [rsi]; al - младший байт rax (в rax - возвращаемое значение функции)
    
    .end:
    inc rsp
    ret 



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; ---
; (1й вх. параметр) rdi - адрес начала буфера
; (2й вх. параметр) rsi - размер буфера
read_word:
    dec rsi; резервируем место под символ конца строки
    mov rcx, 0; rcx - смещение относительно адреса начала буфера
    .loop:
        push rdi; caller-saved
        push rsi; caller-saved
        push rcx; caller-saved
        call read_char
        pop rcx
        pop rsi
        pop rdi 

        cmp rax, 0x20
        je .check_if_spacer_is_leading
        cmp rax, 0x9
        je .check_if_spacer_is_leading
        cmp rax, 0xA
        je .check_if_spacer_is_leading
        jmp .store_in_buf

        .check_if_spacer_is_leading:
        cmp rcx, 0
        je .loop; "игнорируем" начальные пробельные символы
        jmp .buf_ok

        .store_in_buf:
        mov byte [rdi+rcx], al; сохраняем al (младший байт rax) в каждую следующую ячейку буфера

        cmp al, 0; 
        je .buf_ok; в буфер уже записан символ конца строки, поэтому всё нормас

        inc rcx
        cmp rcx, rsi; сравниваем кол-во записанных в буфер символов с макс. допустимым
        jg .buf_overflow

        jmp .loop

    .buf_overflow:
    mov rax, 0
    jmp .end 

    .buf_ok:
    mov rax, rdi
    mov rdx, rcx

    .end:
    ret
 


; 1) 'print_uint' принимал в rdi непосредственно само число, делил его в цикле на 10 и для каждого остатка выбирал и записывал
;     ASCII-код соответсвующего десятичного символа в выходной массив, затем выводил этот выходной массив на экран 
; 2) 'parse_uint' же должен ...
; КОРОЧЕ: 'print_uint' принимал число и выводил символы, 'parse_uint' же принимает указатель на массив символов и преобразует его в число

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx; callee-saved
    mov rcx, 0; счётчик символов
    mov rax, 0; результат

    .loop:
        xor rbx, rbx; rbx(bl) будет содержать индекс, соответствующий какому-то из символов '0', ... '9'
        mov bl, byte [rdi+ rcx]; текущий символ из входной строки -> временную переменную bl
        sub rbx, '0'; ASCII-код превращается в соответствующую цифру
        js .stop_numerical
        cmp rbx, 9
        jg .stop_numerical

        mov r8, 10 
        mul r8; *10
        jo .invalid_string; переполнение при умножении

        add rax, rbx; прибавление рез-ту
        inc rcx                    
        jmp .loop                   

    .invalid_string:
    mov rcx, 0; неудача
    .stop_numerical:
    mov rdx, rcx; возвращаем длину строки в rdx

    pop rbx; callee-saved
    ret 
    


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, byte [rdi] 
    cmp al, '-'
    je .negative
    call parse_uint  
    jmp .end

    .negative:
    inc rdi; пропускаем '-'
    call parse_uint
    cmp rdx, 0
    je .end 
    neg rax; доп. код числа -> rax
    inc rdx; увеличиваем rdx из-за символа '-'

    .end:
    ret 



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; ( принимает:
;   rdi - указатель на строку,
;   rsi - указатель на буфер,
;   rdx - длина буфера 
;   возвращает:
;   rax - длина строки или 0 )
string_copy:
    push rdx
    call string_length
    pop rdx
    inc rax; увеличиваем rax, т.к. в буфер также должен поместиться 0-терминатор
    cmp rax, rdx; сравниваем длину строки с размером буфера
    jg .buf_overflow
    xor rcx, rcx; rcx = 0, 1, 2, ...
    
    .loop:
        mov bl, byte [rdi + rcx]
        mov byte [rsi + rcx], bl
        inc rcx
        cmp rbx, 0; проверка, не является ли записанный символ 0-терминатором
        je .end
        jmp .loop

    .buf_overflow:
    xor rax, rax

    .end:
    ret